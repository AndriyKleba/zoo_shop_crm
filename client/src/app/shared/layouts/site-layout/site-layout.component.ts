import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {MaterialService} from "../../classes/material.service";

@Component({
  selector: 'app-site-layout',
  templateUrl: './site-layout.component.html',
  styleUrls: ['./site-layout.component.css']
})
export class SiteLayoutComponent implements AfterViewInit {

  // @ts-ignore
  @ViewChild('floating') floatingRef: ElementRef;

  links = [
    {url: '/overview', name: 'Огляд'},
    {url: '/analytics', name: 'Аналітика'},
    {url: '/history', name: 'Історія'},
    {url: '/order', name: 'Добавити замовлення'},
    {url: '/categories', name: 'Асортимент'}
  ];

  constructor(private auth: AuthService,
              private router: Router) {
  }

  ngAfterViewInit() {
    MaterialService.initializeFloatingButton(this.floatingRef)
  }

  logOut(event: Event) {
    event.preventDefault();
    this.auth.logOut();
    this.router.navigate(['/login'])
  }
}
