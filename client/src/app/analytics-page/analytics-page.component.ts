import {AfterViewInit, Component, ElementRef, OnDestroy, ViewChild} from '@angular/core';
import {AnalyticsService} from "../shared/services/analytics.service";
import {AnalyticsPage} from "../shared/interfaces";
import {Chart} from 'chart.js'
import {Subscription} from "rxjs";


@Component({
  selector: 'app-analytics-page',
  templateUrl: './analytics-page.component.html',
  styleUrls: ['./analytics-page.component.css']
})
export class AnalyticsPageComponent implements AfterViewInit, OnDestroy {

  // @ts-ignore
  @ViewChild('gain') gainRef: ElementRef;
  // @ts-ignore
  @ViewChild('order') orderRef: ElementRef;

  average: number;
  pending = true;
  aSub: Subscription;

  constructor(private service: AnalyticsService) {
  }

  ngAfterViewInit() {
    const gainConfig: any = {
      label: 'Виручка',
      color: 'rgb(255, 99, 132)'
    };

    const orderConfig: any = {
      label: 'Закази',
      color: 'rgb(54, 162, 235)'
    };

    this.aSub = this.service.getAnalytics().subscribe((data: AnalyticsPage) => {
      this.average = data.average;

      gainConfig.labels = data.chart.map(item => item.label);
      gainConfig.data = data.chart.map(item => item.gain);

      orderConfig.labels = data.chart.map(item => item.label);
      orderConfig.data = data.chart.map(item => item.order);

      //***Gain***
      // gainConfig.labels.push('08.05.2020')
      // gainConfig.data.push(3600)
      // gainConfig.labels.push('07.05.2020')
      // gainConfig.data.push(1600)
      // gainConfig.labels.push('06.05.2020')
      // gainConfig.data.push(4600)
      // gainConfig.labels.push('05.05.2020')
      // gainConfig.data.push(7600)
      //***Gain***

      //***Order***
      // orderConfig.labels.push('08.05.2020')
      // orderConfig.data.push(2)
      // orderConfig.labels.push('07.05.2020')
      // orderConfig.data.push(4)
      // orderConfig.labels.push('06.05.2020')
      // orderConfig.data.push(12)
      // orderConfig.labels.push('05.05.2020')
      // orderConfig.data.push(5)
      //***Order***

      const gainCtx = this.gainRef.nativeElement.getContext('2d');
      const orderCtx = this.orderRef.nativeElement.getContext('2d');
      gainCtx.canvas.height = '300px';
      orderCtx.canvas.height = '300px';

      new Chart(gainCtx, createChartConfig(gainConfig));
      new Chart(orderCtx, createChartConfig(orderConfig));

      this.pending = false;
    })
  }

  ngOnDestroy() {
    if (this.aSub) {
      this.aSub.unsubscribe();
    }

  }


}

function createChartConfig({labels, data, label, color}) {
  return {
    type: 'line',
    options: {
      responsive: true
    },
    data: {
      labels,
      datasets: [
        {
          label, data,
          borderColor: color,
          steppedLine: false,
          fill: false
        }
      ]
    }
  }
}
